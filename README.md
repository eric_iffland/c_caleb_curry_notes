# Notes on the "C Programming All-in-One Tutorial Series (10 Hours!)" by Caleb Curry

https://youtu.be/Bz4MxDeEM6k

These were written in markdown so they are able to read in a repository like this, but I don't usually double space lines so it may be jumbled if reading on gitlab. You can view the source to get a better view of the intended formatting.

These notes were requested by a user in the youtube comments section.

Luke 6:31 "And as you wish that others would do to you, do so to them."

I hope that you get good use out of these notes.

Take care


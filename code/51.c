// the user has to guess a number from 0-5
// output whether or not the person is correct

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int maxValue = 5;
	srand(time(NULL));
	int randomNumber = rand() % maxValue + 1;

	printf("Guess a number 0 - %d: \n", maxValue);
	int guess;
	scanf("%d", &guess);

	if(guess == randomNumber)
	{
		printf("You Win! ");
	}
	else
	{
		printf("You Lost! ");
	}

	printf("Thanks for playing!\n");
	return 0;
}

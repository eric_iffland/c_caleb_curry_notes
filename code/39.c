#include <stdio.h>
#include <stdbool.h>

int main()
{
	float x = 50.0;
	printf("%f", x); //printf takes a double
	// in the above the x is promoted to double, but the original x is not changed
	// %f conversion character works for float and double
	return 0;
}


#include <stdio.h>
#include <stdbool.h>

int main()
{
	printf("How many pizza slices did you eat?\n");
	int slices;
	scanf("%d", &slices);
	int caloriesPerSlice = 250;

	switch(slices)
	{
		case 1:
			printf("You did a great job!\n");
			break;
		case 2:
			printf("You did an OK job!\n");
			break;
		case 3:
			printf("You did a bad job!\n");
			break;
		case 4:
			printf("You are a disssapointment.\n");
			break;
		default:
			printf("You need to eat less\n");
			break;
	}

	printf("You had %d calories.", caloriesPerSlice * slices);
	return 0;
}

#include <stdio.h>

int main()
{
	char name[31]; // must have one more character than what you need for the null terminator. Strings need a character at the end to know it is over.
	printf("What's your name?: ");
	scanf("%s", name); // int x: &x, but dont need address-of operator for arrays
	// printf("Hello Eric!\n"); // normal
	printf("Hello %s!\n", "Eric"); // pass in a string
	return 0;
}

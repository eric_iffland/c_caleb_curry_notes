#include <stdio.h>

int main()
{
    int size = 5;
    int myGrades[size]; //using variable for size
    myGrades[0] = 10; // assigning values to indexes manually
    myGrades[1] = 16;
    myGrades[2] = 17;
    myGrades[3] = 12;
    myGrades[4] = 22;

    for(int i = 0; i < size; i++) //< less than, important
    {
        printf("%d ", myGrades[i]); //use i as size
    }

    return 0;
}

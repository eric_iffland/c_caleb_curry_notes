#include <stdio.h>
#include <stdbool.h>

int main()
{
	bool calebIsFat = true;
	printf("Is Caleb Fat (1 is yes, 0 is no)? %i\n", calebIsFat);

	printf("%i\n", calebIsFat + 10);

	// _Bool caleb_is_fat; //another way to write variables
	return 0;
}


#include <stdio.h>

int main()
{
    int slices = 20;
    int *p = &slices;

    printf("Slices: %d\n", slices);
    printf("Slices through pointer: %d\n", *p);
    
    slices = 21;

    printf("Slices: %d\n", slices);
    printf("Slices through pointer: %d\n", *p);

    *p = 25;
    printf("Slices: %d\n", slices);
    printf("Slices through pointer: %d\n", *p);
    
    slices++; // works
    // *p++; doesn't quite work bc the increment operator has higher precedence over the indirection operator. so what it is actually doing is:
    // *(p++);
    // so what we can do is parenthesis: 
    (*p)++;
	
    return 0;
}

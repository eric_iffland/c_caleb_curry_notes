#include <stdio.h>
#include <string.h>

int main()
{
    int myGrades[] = {12, 23, 40}; //single dimensional array
    
    int const columns = 3; 
    int const rows = 2;
    // cant use variable get:
    // "variable sized object can't be initialized" error
    // also warning: excess elements in array initializer
    int grades[][3] = {
        {12, 23, 34},
        {68, 89, 99}
    };

    // printf("%d ", grades[1][2]);

    for(int i = 0; i < rows; i++)
    {
        for(int k = 0; k < columns; k++)
        {
            printf("%d ", grades[i][k]);
        }
        printf("\n"); // print as a table by adding new line after each row
    }

	return 0;
}

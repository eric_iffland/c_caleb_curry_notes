#include <stdio.h>
#include <stdbool.h>
#include <math.h>

bool isPrime(int input)
{
	
	// for any number between 1 and itself, if it's divisible evenly, then it is NOT prime
	for(int i = sqrt(input); i > 1; i--)
	{
		if(input % i == 0)
		{
			return false;
		}

	}
	return true;
}

int main()
{
	int input;
	printf("Check primality of integer: ");
	scanf("%d", &input);
	// bool prime = isPrime(input);

	for(int i = input; i > 1; i-- )
	{
		bool prime = isPrime(i);
		if (prime)
		{
			//output
			printf("%d Is Prime!\n", i);
		} else
		{
			printf("%d Aint Prime!\n", i);
		}
	}
	return 0;
}

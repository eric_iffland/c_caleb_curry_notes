#include <stdio.h>
#include <stdbool.h>

int main()
{
	bool pizzaIsHealthy;

	int temp;
	printf("Do you like pizza? 1 is true 0 is false: ");
	scanf("%d", &temp);
	pizzaIsHealthy = temp;

	if(pizzaIsHealthy)
	{
		//will this code run?
		printf("Welcome to my pizza app\n");
	}
}

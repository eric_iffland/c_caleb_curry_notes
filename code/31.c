#include <stdio.h>
#include <stdbool.h>

int main()
{
	// + - / *
	// int x = (2 + ((3 * 4) / 3)) - 2; // operator precedence
	int x = 2 + 3 * 4 / (3 - 2); // operator precedence
	// int y = 5 % 2;
	printf("%i\n", x);
	return 0;
}


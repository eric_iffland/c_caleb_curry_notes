#include <stdio.h>

int main()
{
	//int double float
	int dogs = 6;

	//floating point
	// preceision = how much data the variable can hold
	printf("%i %f %f\n", 1, 1111.1111, 1111.1111F); 
	
	return 0;
}

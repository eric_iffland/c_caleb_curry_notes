#include <stdio.h>

int main()
{
    
    // will keep asking until the correct input is entered
    int input;
    do
    {
        printf("Please enter a number 0 -9: ");
        scanf("%d", &input);
    } while (input < 0 || input > 9);

    return 0;
}

#include <stdio.h>
#include <stdbool.h>

int main()
{
	int pizzasToEat = 100;
	// int output = pizzasToEat++; //happens after assignment
	// int output = ++pizzasToEat; //happens before assignment
	int output = pizzasToEat--; //decrement variable
	// int output = --pizzasToEat; //decrement expression
	printf("Output: %i\n", output); //100
	printf("Pizzas to eat: %i\n", pizzasToEat); //99 bc we affixed the decrement opeartor
	return 0;
}


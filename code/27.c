#include <stdio.h>

int main()
{
	_Bool calebIsFat = 1; //camel case, true
	printf("Is Caleb Fat (1 is yes, 0 is no)? %i\n", calebIsFat);

	// _Bool caleb_is_fat; //another way to write variables
	return 0;
}


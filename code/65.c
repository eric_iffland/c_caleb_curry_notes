// doctors office
// 1. add a patient
// 2. view a patient
// 3. search pateints
// 4. exit

#include <stdio.h>

int main ()
{
	printf("Choose a menu option 1-4:\n");
	printf("1. Add a Patient\n");
	printf("2. View a Patient\n");
	printf("3. Search Patients\n");
	printf("4. Exit\n");

	int input;
	scanf("%d", &input);

	if ( input == 1)
	{
		printf("Adding a patient\n");
	}
	else if ( input == 2 )
	{
		printf("Viewing a patient\n");
	}
	else if ( input == 3 )
	{
		printf("Searching a patient\n");
	}
	else if ( input == 4 )
	{
		printf("Exiting, have a nice day!\n");
		printf("Do you want to save? ");
		char q;
		getchar(); // clear the extra newline character from the last scanf out of the input stream (otherwise it will grab the n)
		scanf("%c", &q);
		if(q == 'Y' || q == 'y')
		{
			printf("Alright then! Saving changes!\n");
		} else if(q == 'N' || q == 'n')
		{
			printf("Fine, whatever.\n");
		} else
		{
			printf("Hacking detected, self destructing in 5...4....3....2...1, boop\n");
			printf("...Actually, instead of self-destructing I will delete your data, because that was rude.\n");
			printf("All your data deleted. Have a nice day.\n");
		}
		return 0;
	}
	else 
	{
		printf("incorrect input");
	}

	return 0;
}

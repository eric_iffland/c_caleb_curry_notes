#include <stdio.h>

int main()
{
	printf("What is the temperature outside?\n");
	int temp;
	scanf("%d", &temp);
	int pointsPerDegree = 700;

	switch (temp)
	{
		case 1:
			printf("wow that's low\n");
			break;
		case 20:
			printf("wow that's still low\n");
			break;
		case 100:
			printf("wow that's hot\n");
			break;
		default:
			printf("No real specific statement for that.\n");
			break;
	}

	printf("With a temp of %d, You scored %d points!\n", temp, pointsPerDegree * temp);
	return 0;
}

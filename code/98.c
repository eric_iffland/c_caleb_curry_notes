#include <stdio.h>
// If declared this way, must always prefix with struct:
// struct rectangle
// {
//     int length;
//     int width;
// }
// if declared with typedef, don't have to prefix with struct:
typedef struct
{
    int length;
    int width;
} rectangle; // name goes here now

int main()
{
    rectangle myRectangle = {5, 10};
    printf("Length: %d. Width: %d\n", myRectangle.length, myRectangle.width);
	return 0;
}

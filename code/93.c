#include <stdio.h>
#include <string.h>

void printArray(int arr[], int size)
{
    for(int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }

}

int main()
{
    int myGrades[] = {12, 23, 40}; //single dimensional array
    
    int const columns = 3; 
    int const rows = 2;
    int grades[][3] = {
        {12, 23, 34},
        {68, 89, 99}
    };

    for(int i = 0; i < rows; i++)
    {
        printArray(grades[i], columns);
        printf("\n");
    }

	return 0;
}

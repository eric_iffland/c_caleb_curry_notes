#include <stdio.h>

int main()
{
	printf("How many dogs do you have?: ");
	double dogs;
	scanf("%lf", &dogs); // notice the address-of operator!
	//int double float
	// double dogs = 2.5e4; //hardcoding, generally frowned upon
	printf("%f\n%e\n%g\n", dogs, dogs, dogs); 
	
	return 0;
}

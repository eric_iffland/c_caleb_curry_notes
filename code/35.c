#include <stdio.h>
#include <stdbool.h>

int main()
{
	int pizzasToEat = 100;
	printf("Pizzas to eat: %i\n", pizzasToEat);
	// pizzasToEat = pizzasToEat + 100; //long way to increment larger than one
	// pizzasToEat += 100; //short way
	pizzasToEat += 100; //200
	printf("Pizzas to eat: %i\n", pizzasToEat);
	pizzasToEat -= 100; //100
	pizzasToEat *= 2; //200
	pizzasToEat /= 4; //50
	pizzasToEat %= 5; //0
	printf("Pizzas to eat: %i\n", pizzasToEat);
	return 0;
}


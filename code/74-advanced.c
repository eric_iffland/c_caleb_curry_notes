#include <stdio.h>
#include <stdbool.h>
#include <math.h>

int main()
{
	int input;
	printf("Check primality of integer: ");
	scanf("%d", &input);
	bool isPrime = true;
	
	// for any number between 1 and itself, if it's divisible evenly, then it is NOT prime
	for(int i = sqrt(input); i > 1; i--)
	{
		printf("%d ", i);
		if(input % i == 0)
		{
			isPrime = false;
		}
	}
	if (isPrime)
	{
		//output
		printf("\nYea boiiiii it's Prime!\n");
	} else
	{
		printf("\nOh nooooooo, it aint prime!\n");
	}
	return 0;
}

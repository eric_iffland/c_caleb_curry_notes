# How to write if Statement with bool

scanf() `%d` will take an integer. It is generally preferred to use `%d` rather than `%i` if you want to force only integers rather than e.g. hexadecimal.

Branching allows us to have two completely different outputs based on the inputs...

If you put a number in an if statement by itself it will be true as long it is not 0. In C, only 0 is false, all other numbers are seen as true.



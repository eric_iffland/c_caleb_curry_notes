# Intro to Logic

## Branching
When a program splits.
At the split, there is an expression.
A Boolean expression is true/false.

## Boolean Logic
boolean logic is integrated into a lot of fields not just computer science.

is `age > 13` ?
if yes, they have access
if no, they do not

You can do branching by boolean logic expressions as many times as you want, but sometimes it's easier to create a compound expression.

## Compound Expression
2 or more expressions combined together

## Truth Tables
| `age > 13` | `gender = F` | `age > 13` && `gender = F`
| T           |  T          | T <-- only condition
| T           | F           | F
| F           | T           | F
| F           | F           | F

In this truth table, the top condition is one branch, and the other 3 are another branch.

&& is a logical operator that allows us to combine multiple expressions.

## Control Flow Statement
a computer science term for branching
others: if, for, while


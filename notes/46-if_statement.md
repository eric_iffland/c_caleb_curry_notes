# If Statement

```
if( expression )
{
	//code
}
```

Expression gets evaluated as true or false (boolean expression)

This is often called a conditional, because the code in the curly braces is only executed if a certain condition is met.

Some people call the code within the curly braces a "code block". Differentiating like this is necessary because the code inside can behave differently than code outside of it, and this is due to Scoping.

## Examples
`if(CalebIsFat)`
Because CalebIsFat is a boolean variable, we don't have to add anything else to this expression to make it boolean, like "CalebIsFat > 0", "CalebIsFat == true" etc.

Essentially this will look like one of these:
if(true) or if(false)
and the code in the curly braces will only get executed on the true case.

## Key takeaway
If you have a boolean variable, you don't have to do anything except put the name of the variable in the if statement's parenthesis.






comments are two forward slashes

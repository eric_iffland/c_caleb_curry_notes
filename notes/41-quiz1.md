# Quiz 1

Whitespace insensitive languages don't care about spaces.

`int x` = variable declaration
syntax = the way everything is typed out, what goes where, etc.

printf() and scanf() are examples of functions, specifically IO functions.

Putting something into a function is called "passing".
The thing we pass in is called an argument, and that is assigned to a parameter within the function.



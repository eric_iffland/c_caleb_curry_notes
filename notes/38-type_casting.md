# Type Casting

e.g. 4 quarters = $1
someone loans you a dollar bill and you pay them back 4 quarters. But they want the dollar bill that they gave you back not quarters, so they go to a bank and exchange the quarters for a dollar bill. In computer science terms, you can say that the dollar was type casted from double to integer (a loose example here..).

C is very picky when it comes to data types so we often need to type cast.

## 2 main categories of Type Casting
- Implicit
	- happens automatically, result is "lossless"
	- 1 -> 1.00
- Explicit
	- have to tell the computer to do it, and it is "lossy"
	- 1.84 -> 1
	- ex: `(int)3.5`





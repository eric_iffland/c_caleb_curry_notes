# Evaluating Complex Conditionals

`int x = 5`
`x < 5`
`5 < 5` false
`x > 1` true
`x > 1 && x > 2` true

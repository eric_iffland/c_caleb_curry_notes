# Intro to Loops

Loops are dangerous because they can become infinite loops. So remember the basic syntax:

## Initialization
`i = 0`
## Comparison
do the update until we get to:
`i <= 10`
## Update
`i++`

Remember as: ICU

## Types
- for
- while
- do while


# Good Coding Practices

## Indenting
Indent after every new curly brace

Easiest way to avoid issues is to create the structure of the code first, before any issues can arise.

E.g. three if statements: Make all three including both curly braces then start typing the code blocks

## Comments
Just be consistent where you put your comments, whether above, below or the right - stick with one. Or, general explanation above, then details on the right. But have a  consistent commenting convention.
multiline or inline comments: `/* comment */` 
afterline comments: `// comment`

## Curly braces
It is easier to keep track of things if your starting brace and ending brace are on the same column!



# More Advanced Loops

## Decrement
```
int max
scanf("%d", &max);

for (; max >= 0; max--)
{
	printf("%d\n", max);
}
```

## Decrement by 2
```
int max
scanf("%d", &max);

for (; max >= 0; max = max - 2)
{
	printf("%d\n", max);
}
```

## Infinite loops
can happen sometimes when you don't think of all the possible inputs humans can enter
For instance if your condition is something like: `for(; max != 0; max = max -2)` and a user entered 11, not an even number, it would skip the number 0 and then count down forever into the negatives until the program was forcefully terminated.



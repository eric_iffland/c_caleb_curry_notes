# Multiple If vs. Else if

## Two in a row:
both may execute
```
if()
{

}
if()
{

}
```
## If else
one or the other may execute
```
if()
{

} else
{

}
```

### Example

```
if(age > 18)
{

}
if(age > 65) // else if(age > 65)
{

}
```

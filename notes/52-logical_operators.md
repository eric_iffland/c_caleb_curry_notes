# Logical Operators

Logical Operators are used to build complex conditionals.

```c
if(email is verified OR phone is verified)
{
	//grant access
}
```
It is convention to capitalize `OR`. In C it is written as two vertical bars `||`.

## Ones to know
`||` = OR
`&&` = AND
`!` = NOT

The logical operators enable the other pieces of the expression within the  if statement to contribute to a bigger picture.

## Short circuiting
When an if statement with a complex conditional is done evaluating early before finishing evaluating the rest of the items in the complex conditional.

You can use truth tables to understand the complex conditionals.

## XOR
Exclusive OR
Another logical operator: `XOR` - but this isn't in C exactly. There is a way to mimmick it though.
`A != B` - they can't both be true, it has to be one or the other.

The name of the other OR is just OR, but to be more specific, you would say it is Inclusive OR, meaning it can be one or othe other, or both.

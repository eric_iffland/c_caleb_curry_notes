# How to Code a For Loop

see code file

To declare the variable within the for statement you need to be on a later version of C - doesn't say which.
If you are on an older version, you can just initialize the variable ahead of time:
```
int 1;
for (i = 0;...) 
```
otherwise:
```
for(int 1 = 0; i < 10; i++)
{
	printf("%d", i);
}
```

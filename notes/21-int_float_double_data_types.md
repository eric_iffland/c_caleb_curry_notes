# Int Float Double

## Int
integer
an integer is a whole number
1 5 8 9 3 -4 0

## Float
both float and double are considered floating point numbers
a floating point is any number with a fractional part (e.g. 3.8, 6.0, 7.9993 etc)

## Double
both float and double are considered floating point numbers
a floating point is any number with a fractional part (e.g. 3.8, 6.0, 7.9993 etc)

If not sure, err on the side of using double instead of float.
Double, can store double the precision as a float.
Functions also convert floats into doubles, so they are more "standard". This also explains why there is one conversion character for both float and doubles. `%f`

There is a time to use floats though:
For memory conservation.

## Base 2
Computers store numbers in base 2 (binary)
You can't trust floating point numbers for everything, it can get it wrong. Integers are more trustworthy.

"When unsure, prepare for trouble, make it double"

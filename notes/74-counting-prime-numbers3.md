# Counting Prime Numbers pt.3

```
bool isPrime = true;
for(int i = 2; i < input; i++)
{
	if(input % i == 0)
	{
		isPrime = false;
	}
}
if (isPrime)
{
	//output
}
```

Using the modulus operator, we only have to go up to the square root.
And we don't have to try all numbers we only have to try the prime factors.

# C Basics Part One

## Spacing
```
#include <stdio.h>

int main()
{
	int x; // variable declaration (of type int)
	x = 5; // variable initialization
	// syntax = form or format of the language
	int y = x / 2; // y will equal half of 5
	// the above is a declaration and initialization in one line
	printf();
	return 0;
}
```

C is a space insensitive language.
Line breaks don't matter, etc.
Should indent in code blocks for your own sake.

## Expression
Anything that can be evaluated (to 1 value)
```
int y = x / 2;
```
x/2 there is an expression

# Working with Arrays

You can assign variables to array indexes manually, one at a time:
```
int myGrades[10];
myGrades[0] = 10; //index 0
myGrades[1] = 11;

printf("%d ", myGrades[0]);
```

Or, you can assign variables to the array all at once:
```
int myGrades[] = {10, 11, 12, 14, 16, 18, 234, 23, 32};
```
Here, the compiler will determine the static size of the array by using the amount of variables assigned to it - so we don't have to manually enter the amount when assigning this way.

## Terms

* Index
    + A zero based number, associated with each element (like an ID)
* Element
    + An item in the array. "first element" is the element with index 0.
* Size
    + Number of elements in the array, not zero based! Typically you will have a size variable for the size, otherwise it can be difficult to keep track of it. This has to do with decay; when you pass an array it turns into a pointer (deeper subject for later).

`int x = myGrades[4] + 10;

## When to use brackets
If you want to reference an entire array you just say: `myGrades` without brackets.
You only use brackets when you are declaring an array or are referencing some index of that array.
Or rather, the only time you don't use brackets is when you are referencing the entire array.

## Outside the bounds

When you go outside the bounds of the static array, you may get an error saying it is past the index of the array, or some undefined behavior (crashes, craziness).

# Relational/Comparison Operators

`if(pizzaRating > 6) {}`
## Important operators
`>` - greater than operator
`<` - less than
`>=` - greater than or equal to (different than math symbol)
`<=` - less than or equal to
`==` - equal. It is two equals bc a single = is an assignment operator.
`!` - negation operator. will flip value of anything. `!fun`
`!=` - not equals (one = just because)

## Example
As you build more complex expressions you have to understand the precedence of these operators.

`pizzaRating > 3 + 3` - a little bit confusing
The plus sign has a higher precedence, which means it happens first, so this actually is `pizzaRating > 6`


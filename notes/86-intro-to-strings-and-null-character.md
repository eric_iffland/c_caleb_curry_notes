# Intro to Strings and Null Character

`"hello"` - in order to assign this to a value to a variable we would need a character array.

`char a[10] = "hello"`
`|h|e|l|l|o|\0`
backslash 0 here is the null character used to terminate the string. If you don't have that, it will be like trying to access parts of an array that don't exist; unexpected results and errors.

When getting user input, you never know the size they are going to give you, so you just want to make sure you are prepared for different sizes of strings.
E.G. with the scanf function:
`scanf("%9s", ` - this will limit `%s` by 9

for single arrays, let the compiler decide how long a string array should be: `char a[] = "hello"`
The only time you would want to put a value there, is if you're not assigning a string constant; like you are getting it from input or something, then make sure your string size is set to 10 for example and only accept 9 from the user to leave room for the null character.





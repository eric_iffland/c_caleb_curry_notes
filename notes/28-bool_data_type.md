# Bool data type

## stdbool.h
- Include stdbool.h in order to use better looking bool data type declarations. ( `bool` vs `_Bool` )
- Can also use keyword true and false. These are keywords and not strings, so do not quote them. 
- The value stored are still 0 or 1.





Q: What is casting

Q: will value be truncated or rounded
A: It will be truncated (5.9 becomes 5)

Q: In an array what is the last the last character you put in to make it a string?
A: Null character, or Terminating character, Null Terminator. looks like this: `\0`

Q: What's the difference between a primitive data type and a complex data type?
A: Primitive are foundational to the language, they come by default. You can use them as building blocks to make Complex data types. Complex data type: coordinates (2 integers), linked list, or other data structures.

Q: When you are printing, what is the format character for an integer?
A: `%d` OR `%i`

Q: What are the format characters for floating point values?
A: `%f`, `%g`, or `%e`

Q: When using scanf(), what is special about the value you pass in to it? (aka the argument)
A: Needs address-of operator prefixed to variable. `&var`. That means we're passing in the address of the variable not the value.

Q: What is the ASCII code for A?
A: 65. If you don't know you can look up the ascii table or start printing different characters as integers in C.

Q: What is the difference between 'A' and "A"?
A: Double quote is a string, single quote is a character. Both are constants.



# Variables

Temporarily store some value

Declaring is when you say, hey this variable exists.
Initializing is when you assign a value to that variable.

`int x, x=5`
The equals sign here is an assignment operator.

You always want to be sure to initialize a variable before using it. Otherwise it will output garbage.

## Allowed characters
A-Z, a-z, 0-9, _
* Variable names are case sensitive
* tacos, tAcos, TaCOs are all possible, but it will be confusing
* stick with one style of naming
* if you do need to use a name twice, you could use an underscore prefixed.
* you want names to be short in length, but describe things well.
* amount vs amountSold
* amtSold - over time the meaning of shortened words is lost, making code harder to read. 3 letters isn't really worth it.
* the_number_of_cars_sold - too long, "the" is dumb
* int_cars_sold - makes just as much sense but is shorter




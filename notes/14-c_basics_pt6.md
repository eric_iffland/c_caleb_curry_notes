# conversion character

`%i` - integer
`%s` a string, sequence of characters

# Arrays
`char name[31];` - brackets define an array. The number is the length of the strings in the array.
must have one more character than what you need for the null terminator. Strings need a character at the end to know it is over, and that is called the null terminator. So, need to reserve room for that.

# Calling Arrays
`scanf("%s", name);`
int x: &x - integers need the address-of operator
but don't need address-of operator for arrays, it's an exception.


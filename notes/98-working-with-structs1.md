# Working with Structs

A struct is multiple variables in one variable; a group of variables that belong together.

## declaring a struct
```
struct <name>
{

}
```
or you can use typedef when declaring, and then you don't have to prefix it with struct when instantiating one later.
```
typedef struct
{
    int length;
    int width;
} rectangle; // name goes here now
```
## instantiating structs
`rectangle myRectangle = {5, 10};`

## calling structs
Structs can be called with dot notation
`myRectangle.length`



# ASCII

char is short for character

characters = '1' '2' '3' '4'
numbers = 1 2 3 4

## Character sets - allowed characters
### ASCII

binary = two states, 0 1
one binary character is a bit
a group of 8 bits is so common it has its own name, a byte
computers understand groups of bits to represent things in the computer
`01000001` = 'A' in ASCII
standard ASCII actually only uses 7 of the 8 bits
some versions of ASCII use the last bit and those are the extended ASCII character sets

since there's only 7 spots, there's only so many possible combinations: this number is 128; so ASCII supports 128 characters.
The computer can interpret the ascii byte as a number instead, and that is where we get the correlating numbers to each letter.
A if it had its byte interpreted as a number, is 65.
Every character has some numeric value behind it.




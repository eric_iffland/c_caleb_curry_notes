# Creating Void Functions

A void function in general, isn't used to calculate something, it's used to DO something, like output to console etc.

Voids don't return anything.
You CAN use "return" if you want to do like a psuedo-break, or something...
```
if(1)
{
    return;
}
```


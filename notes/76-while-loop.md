# While Loop
The while loop can be used to achieve everything a for loop can.
The way you structure it is different, a little more flexibility with a while loop.

```
int i = 0; // initialization
while(i < 10) //comparison
{
	printf("%d", i);
	i++; //update
}
return 0;
```
The update happens at the end (or, in the code block somewhere), which makes the while loop a little more clear.

The flexibility in the while loop is that you can update/increment wherever you want in the code.

When you don't know how long something will execute, you may want to use a while loop.
When you have an exact number it might be more clear with a for loop.

# Intro to Pointers and Indirection Operator

Pointer is a way to point to a location in memory.
`int x = 5`
First you tell it what data type you want to point to
`int *P` Asterisk indicates a pointer, and then there is a variable, typically it is `P`
Then we use the address-of operator that will get the address of a variable in memory:
`int *P = &x;`
This will create a variable P and it's going to point to `5`

It's kind of like having two variables, and they can both be used to reference the same data.
There is the variable system, and then there is the memory system.

## Indirection Operator
Asterisk
To reference a pointer it is the same syntax as when you are declaring a pointer just without the data type.
`*P`

## Things to know
* How to declare pointers `int *P`
* How to assign pointers `P = &x`
* How you reference the value (with indirection operator) `*P`, also known as "dereferencing", getting the value that the pointer points to.





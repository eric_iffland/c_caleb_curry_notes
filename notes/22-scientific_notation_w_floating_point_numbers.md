# Scientific notation with floating point numbers

Floating point numbers are stored in scientific notation

## Scientific notation

take a number and multiply it by 10 to some power.

e.g.

24000 would be:

2.4 * 10^4

e.g.

.000045

4.5 * 10^-5

## Sometimes it's easier to enter our data in scientific notation. and in C that looks like this:

`2.5e4` - here e is short for `* 10`

so this is 2.5 * 10^4, which is 25,000

2.5 * 10^1 = 25.

2.5 * 10^2 = 250.

2.5 * 10^3 = 2500.

2.5 * 10^4 = 25000.

the decimal moves right for each power of...





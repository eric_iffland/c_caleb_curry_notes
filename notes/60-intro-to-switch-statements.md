# Intro to Switch Statements

Switch is similar to If() but different in structure.
The purpose of switch is to branch based on conditions.
When you use if() or switch depends on what you are trying to accomplish.

## Good for
Switches are good for comparing simple values.
If statements are good for complex expressions.
Switch cannot have comparison or logical operators.

`int level = 1;`
```
switch(level)
{
	case 0:
		//code
	break;
	case 1:
		//code
	break;
	default:
		// code
	break;
}
```
Default is not required. Default is similar to "else" in an if() statement in that it is a "catch-all" if no other conditions are met.
Accidentally not writing a break statement is called "fall through"


# Characters in switch

With a switch statement, you can use an data that is directly convertible to an integral data type (integer/int). 
- integers
- characters
- enumerations

Enumeration is a data type that lets you choose from various options...will cover later.

## You can have multiple cases for one code block by purposefully doing fall-through.

```
case 1:
case 1:
case 3:
	//code
	break;
```

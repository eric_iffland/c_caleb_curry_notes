# Single Line If Statement

`if(true) printf("TRUE\n");`

If statements without curly braces only execute the first line as part of its code block, subsequent lines will be just normal code.

So you COULD do this:
```
if(true)
	printf("TRUE\n");
```

But don't....

If you are going to do one of those, keep it on the same line - might as well, it's just as legible.



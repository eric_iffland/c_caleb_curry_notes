# Unix and linux commands
`pwd` - print working directory
`ls` - list directory contents
	in ls, two special files are listed:
	. = a reference to the directory you are in (the working directory)
	.. = the parent directory
`cd` - change directory
	- can take a relative or absolute path

home = ~

other linux basics...

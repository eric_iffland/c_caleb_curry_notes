# Else If Statement

With an If Else Statement the program branches 2 different ways.
With an Else If the program can branch 3+ different ways.

```
if( temp < 50 )
{
	// code
} else if ( temp > 90 )
{
	//code
} else
{
	//code
}
```

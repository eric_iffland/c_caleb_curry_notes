# Short Circuit Evaluation

Saves computing time by not evaluating the rest of the expression.

True || x = True
False || x = x
True && x = x
False && x = False

Boolean Simplification - making true/false conditionals shorter

Can also put functions inside an if statement:
`if( emailVerified() || phoneVerified() )`


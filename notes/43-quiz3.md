# Quiz 3

Q: What is this operator called? `%`
A: Modulus operator. It finds the remainder. Can be used for lots of things, like finding even and odd.

Q: What is the difference between i++ and ++i?
A: procedurally adds to value i. i++ increments the variable after it uses i. ++i increments i before i is used. This could be before and after assignment to another variable, use in a function, etc.

Q: What does this mean? `J += 5;`
A: Shorthand for `J = J + 5;` or for doing `j++` 5 times.

Q: What is the term used to describe what order operators are executed in?
A: Operator precedence

Q: If you have two operators with the same precedence, what is the term to describe which way they are evaluated in?
A: Associativity. (left to right, right to left). Different operators have different associativity. E.x. the division operator associativity is left to right.

Q: C is a ______ typed language.
A: Strongly. Meaning it is strict when it comes to data types, and every single value has to have a data type.

Q: If we want to use printf() or scanf() in our code, what do we have to do?
A: `#include <stdio.h>` at the top of the file.

Q: If you want to use the bool data type (instead of `_Bool`)what do you have to do?
A: `#include <stdbool.h>`











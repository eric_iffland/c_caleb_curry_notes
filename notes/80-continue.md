# Continue

The continue keyword skips to the end of the iteration (doen't stop the whole loop like the break keyword).

It is common that continue and break will be used together.

Useful for when you want execute some new code and skip the normal code during a condition inside a loop.

see code file

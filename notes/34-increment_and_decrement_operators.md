# Increment and Decrement Operators

two of the most important operators

```c
int pizzasToEat = 123;
pizzasToEat = PizzasToEat + 1; // hard way to increment
```

The shortand way to do this is:
`pizzasToEat++;`

The first is fine for adding more than one.
If just adding one, use ++.

++ is a unary opeartor, meaning it only takes/changes one variable.

## Placement
++ or -- after the variable affects the variable after assignment.
prefixing the variable with ++ or -- affects the expression BEFORE it assigned.

```c
int pizzasToEat = 100;
// int output = pizzasToEat++; //happens after assignment
// int output = ++pizzasToEat; //happens before assignment
int output = pizzasToEat--; //decrement variable
// int output = --pizzasToEat; //decrement expression
printf("Output: %i\n", output); //100
printf("Pizzas to eat: %i\n", pizzasToEat); 
```
The above output will be 100 and pizzasToEat will be 99

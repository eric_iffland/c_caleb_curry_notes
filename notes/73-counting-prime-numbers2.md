# Counting Prime Numbers pt. 2

```
for (; input >=2; input--)
{
	// figure out if prime
	if num is prime
		print all prime numbers
		numPrime++;
}
```

## How to figure out if a number is prime?
my note before starting: probably check remainder with modulus, check if divisible by 2

Any number divided by itself will give us 1.
Any number divided by 1 will give us the original number.
It's the numbers between 1 and N that we can use to check if the number is prime.
So if we take 7/6, 7/5, 7/4, 7/3, 7/2 - there's something in there that will tell us whether or not 7 is prime. 
A prime number is only divisible by a whole number with itself and one.
All other numbers we divide by will give us a remainder when doing integer division.
For example, if we had 2 people and 7 slices of pizza, we would end up with one left over. If we have 3 people, we still have a remainder. etc. (primes can't be distributed evenly)
If we modulus the numbers below N except 1 and it equals 0 for any set, then the number is NOT prime.
For example `6%2=0`, so 6 is not prime.

```
bool isPrime = true;
for(int i = 2; i < input; i++)
{
	if(input % i == 0)
	{
		isPrime = false;
	}
}
if (isPrime)
{
	//output
}
```

This will work except for some edge cases. If doing for production, there is a way to do this with sqrt() that is more bulletproof.

# Working with Pointers

see code file

## incrementing pointers
```c
    slices++; // incrementing a normal variable
    // *p++; doesn't quite work bc the increment operator has higher precedence over the indirection operator. so what it is actually doing is:
    // *(p++);
    // so what we can do is some parenthesis in our expression to help define what we mean so the compiler understands: 
    (*p)++;
```

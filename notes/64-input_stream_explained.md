# Input Stream Explained

scanf() is not the best for protecting against user input issues. But scanf() is fine for learning.

If an application asks our age, the actual characters placed into the input stream would be: `32\n`
The \n is where we hit enter.

Then if we assign that input to a variable:
`scanf("%d", &d);`
The \n newline character is left over in the input stream.
When dealing with numbers, this is fine.
However an issue arises if the next input is a character type. The leftover n will be seen as a character and added to the input.

When using scanf() multiple times in your program, sometimes you have to flush the input stream and get rid of any newline characters, and you can do that with a function: `getchar()`

getchar() gets the one n character from the input stream (like how we get the actual input and assign it to a variable), it "does" something with it so that it is taken out of the stream.

HW: explore some other input functions, see how they are different from scanf()

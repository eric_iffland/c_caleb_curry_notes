# Break

The break keyword came up earlier in switch statements.
Essentially it stops you from falling through.

It has another use though, and that is to get out of loops.

### E.g. with a for loop
on the 7th iteration the loop will stop:
```
for (int i = 0; i < 60; i++)
{
    printf("Helloo\n");
    if (i == 6) break;
}
```

### E.g. with a while loop
will loop forever until a condition is met
```
while(1) // 1 = true, remember, so this will loop forever
{
    // do something indefinitely
    if () // if this condition is met though..
    {
        break; // stop the loop
    }
}
```


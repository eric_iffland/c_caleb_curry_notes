# Refactoring

Changing the code without changing the functionality.

## Reasons to refactor
* might have a new or optimized way to write the code
* clean up code

Before you refactor a whole project you will want to have some sort of testing set up to ensure your codebase still functions; it is easy to break other parts of the code without realizing it when refactoring.

> Took 85.c and copied to 93.c to do some refactoring.

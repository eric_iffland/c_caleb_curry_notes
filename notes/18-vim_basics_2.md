# Vim basics 2
Fri 17 Jul 2020 09:08:41 AM PDT

`ZZ` - force save and exit
`ctrl + z` - suspend vim
type `fg` in the terminal to resume. as in "foreground"


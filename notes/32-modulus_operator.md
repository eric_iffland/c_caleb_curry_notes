# Modulus Operator

`%` percent sign

Gives you the remainder.
Imagine you have a pizza with 5 slices, and you share 2 with one person, 2 for you. You have one left over.

In order to use modulus, you have to be dealing with integers. Not doubles. (doubles would never have a remainder)

## What can we use modulus for?
- Figure out whether something is even or odd
`17 % 2 = 1`
An odd number is 2 * any number plus one. `2(k) + 1`
An even number is `2(k)` (2 * any number)




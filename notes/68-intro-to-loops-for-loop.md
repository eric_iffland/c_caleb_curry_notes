# Intro to Loops 2

## For loop
typically people use i as the variable to stand for iteration

```
for(int i = 0; i <= 10; i++)
{
	//code
}
```
If condition is true, code is executed, and then the update happens
This repeats until the condition is false.

## Uses
You can iterate through arrays by using i as the array index: `array[i]`
You can use this to count by printing i


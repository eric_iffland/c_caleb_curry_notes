# Comments in C

## Single line comments
`// any text lalala`
two forward slashes create a single line comment

Single line comments can be placed AFTER code in the same line

## Multi-line comments
```
/* lalal
lalal
lalal
*/
```
Multiline comments can be placed inside a line of code


A lot of people put multiline comments in the head with information about the file.
```
/*********
Author: Dumb
Description: best app ever
Date: stupid
*********/
```
Author and date is covered by version control much better

Less comments may be better sometimes.
If you have a lot of comments you are less likely to remember to update them when changing code, and later a misleading comment can make work HARDER not easier.
The point of the comment is so you know what some code does, not necessarily how or why.

## Todo comments
`//TODO something`
These are nice because you can search for them and/or other people have an idea what needs to be done

# Function Design

`printf( arguments )`

We can combine functions into a Library, and then include that Library in new projects.

The idea behind functions is to be DRY - don't repeat yourself.

Instead of molding plastic every time, have the legos already made.

## Things to know about function design
- Don't output (e.g. to the conosle)
    - unless the purpose is to do so like in dev/test
    - print on the calling side not the function side
- One use
    - the name of the function should describe what it does, and it should not have "and" in the name...for example
    - the purpose of a function should be to solve one problem, then have a collection of functions to solve a problem
    - as your function becomes bigger, it becomes less useful
    - then you start to develop a library to do more complex things by combining them

